const path = require('path');
const mkdirp = require('mkdirp');
const fs = require('fs');
const Database = require('better-sqlite3');

const init = async (dir) =>{
  //create folders
  const folders = [
    '/server/application/config',
    '/server/application/logs',
    '/server/application/classes',
    '/server/application/views',
    '/server/modules',
    '/server/system',
    '/db',
    '/server/tmp',
  ]

  for(let i=0; i<folders.length; i++){
    const folder = path.normalize(dir + folders[i]);
    await mkdirp(folder);
    console.log('create folder: ' + folder);
  }

  //create bootstrap
  const bootStrap =
    `module.exports = {
  modules: [],
  system: []
};
require('@komino/komod-route');
`;

  const main =
    `
const path = require('path');
const Server = require('@komino/fastify-minimal-setup');

const m = new Server(null, {
  salt: '&E)H@McQeOPWmZq4t7w!z%C*F-JaNdRg',
  secureCookie: false,
  maxAge: 86400000,
  sessionDBFolder: path.join(__dirname, '/sessions/db'),
  logger: false,
});

const {K8, Controller} = require('@komino/k8');
K8.init(__dirname);

const {RouteList} = require('@komino/komod-route');

class ScaffoldController extends Controller{
  async action_index(){
    this.body = "hello world!!!"
  }
}

RouteList.add('/', ScaffoldController);
RouteList.createRoute(m.app);

m.listen(8081);
`;

  setTimeout(()=>{
    const sessionDB = path.normalize(dir+'/db/sessions.sqlite');
    fs.writeFileSync(sessionDB, '', {encoding: 'utf8'});

    const db = new Database();
    db.exec(`CREATE TABLE sessions(
    id INTEGER UNIQUE DEFAULT ((( strftime('%s','now') - 1563741060 ) * 100000) + (RANDOM() & 65535)) NOT NULL ,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL ,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL ,
    sid TEXT UNIQUE NOT NULL ,
    expired INTEGER DEFAULT 0 NOT NULL ,
    sess TEXT
);
CREATE TRIGGER sessions_updated_at AFTER UPDATE ON sessions WHEN old.updated_at < CURRENT_TIMESTAMP BEGIN
    UPDATE sessions SET updated_at = CURRENT_TIMESTAMP WHERE id = old.id;
END;
CREATE UNIQUE INDEX idx_sessions_sid ON sessions (sid);`);

    fs.writeFileSync(path.normalize(dir + '/server/application/bootstrap.js'), bootStrap,{encoding: 'utf8'})
    fs.writeFileSync(path.normalize(dir + '/server/main.js'), main,{encoding: 'utf8'})
  }, 100)

}

module.exports = async (dir) =>{
  await init(dir);
}