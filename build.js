/*scaffold(
  `${__dirname}/../default/site/db/content.sqlite`,
  `${__dirname}/exports/content/schema.sql`,
  `${__dirname}/../server/modules/shop/classes/`,
  require('./schema-content')
);*/

/*
scaffold(
  `${__dirname}/../default/site/db/sessions.sqlite`,
  `${__dirname}/exports/session/schema.sql`,
  `${__dirname}/exports/session/classes/`,
  require('./schema-session')
);*/
const pluralize = require('pluralize');
const DateBase  = require('better-sqlite3');
const { parse, insert, schemaHeader } = require('@komino/graphql-sqlite-ddl');
const { codeGen } = require('@komino/graphql-k8-scaffold');
const { buildSchema } = require('graphql');
const fs = require('fs');
const mkdirp = require('mkdirp');
const path = require('path');

const readFileOptions = {encoding:'utf8', flag:'r'};
const interfaces = fs.readFileSync(__dirname + '/graphQL/_interfaces.graphql', readFileOptions);

async function build(GraphQL_Path, SamplePath, SQL_Path, DB_Path, classPath){
  await mkdirp(path.dirname(SQL_Path));
  await mkdirp(path.dirname(DB_Path));
  await mkdirp(path.normalize(classPath));
  let schema, sql, db;

  try {
    const typeDef = fs.readFileSync(GraphQL_Path, readFileOptions);
    schema = buildSchema(schemaHeader + interfaces + typeDef);
  }catch(e){
    console.log('Error build schema');
    console.log(e);
    console.log(GraphQL_Path);
  }

  try{
    sql = parse(schema);
    fs.writeFileSync(SQL_Path, sql, {encoding: 'utf8'});

    //delete db
    if(fs.existsSync(DB_Path))fs.unlinkSync(DB_Path);
    fs.writeFileSync(DB_Path, '', {encoding: 'utf8'});
    db = new DateBase(DB_Path)
    db.exec(sql);
  }catch(e){
    console.log(e);
    console.log('Error parse SQL');
    console.log(GraphQL_Path);
  }

  try {
    if (SamplePath !== '') {
      const samples = require(SamplePath);
      const inserts = insert(samples);
      db.exec(inserts);
    }
  }catch(e) {
    console.log(e);
    console.log('Error insert sample data');
    console.log(SamplePath);
  }

  try{
    const classes = codeGen(schema);
    classes.forEach((v, k)=>{
      fs.writeFileSync(classPath + '/' + pluralize.singular(k) + '.js', v, {encoding: 'utf8'})
    })
  }catch(e){
    console.log(e);
    console.log('error generate classes')
    console.log(classPath);
  }

}
module.exports = build;